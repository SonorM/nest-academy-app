import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { NavigationRoutes } from '../../navigation/navigation-params';
import { NestLogo } from '../icons';
import { Bell } from '../icons/bellicon';
import { RedIndicator } from '../illustration/redindicator';
import { Box } from './box';
import { Spacing } from './spacing';

export const Header: React.FC<any> = ({ Notification }) => {
  const navigation = useNavigation();
  const navigate = () => {
    navigation.navigate(NavigationRoutes.NotificationScreen, {
      notification: Notification,
    });
  };
  return (
    <Spacing ph={3}>
      <Box alignItems='center' height={56} justifyContent='center'>
        <Box flexDirection='row' justifyContent='space-between' width='100%'>
          <NestLogo />
          <Box>
            {Notification.length === 0 ? (
              <TouchableOpacity onPress={() => navigate()}>
                <Bell />
              </TouchableOpacity>
            ) : (
              <Spacing ph={3}>
                <TouchableOpacity onPress={() => navigate()}>
                  <Box
                    position={'absolute'}
                    alignItems={'flex-end'}
                    width={'100%'}
                    zIndex={1}
                  >
                    <RedIndicator />
                  </Box>
                  <Bell />
                </TouchableOpacity>
              </Spacing>
            )}
          </Box>
        </Box>
      </Box>
    </Spacing>
  );
};

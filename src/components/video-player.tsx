import React, { useEffect, useRef, useState } from 'react';
import { StyleSheet } from 'react-native';
import {
  PanGestureHandler,
  State,
  TouchableHighlight,
} from 'react-native-gesture-handler';
import Animated from 'react-native-reanimated';
import Video from 'react-native-video';
import { Border, Button, Text } from './core';
import {
  FullScreenIcon,
  LeftControlIcon,
  PauseIcon,
  ResumeIcon,
  RightControlIcon,
} from './icons';
import { Box, Queue, Stack } from './layout';

interface Props {
  source: any;
  width?: number;
  repeat?: boolean;
  pause?: boolean;
  height?: number | string;
}

export const VideoPlayer: React.FC<Props> = ({
  width = 375,
  height = 260,
  repeat = false,
  pause,
  source,
}) => {
  var videoRef = useRef<any>();
  const [progress, setProgress] = useState<number>(0);
  const [duration, setDuration] = useState<number>(1);
  const [fullScreen, setFullScreen] = useState(false);
  const [dragging, setDragging] = useState(false);
  const [visible, setVisible] = useState(false);
  const [timer, setTimer] = useState(false);
  const [isPaused, setIsPaused] = useState(true);

  useEffect(() => {
    if (visible) {
      setTimeout(() => {
        setVisible(false);
      }, 8000);
    }
  }, [visible]);

  useEffect(() => {
    if (pause !== undefined) setIsPaused(pause);
  }, [pause]);

  const getTime = (time: number) => {
    const minute = parseInt(`${time / 60}`);
    const second = parseInt(`${time - minute * 60}`);

    return `${minute}:${second}`;
  };

  const Controls = () => {
    return (
      <Box
        width={'60%'}
        flexDirection={'row'}
        justifyContent={'space-between'}
        zIndex={2}
      >
        <Button
          category={'text'}
          onPress={() => setIsPaused(!isPaused)}
          width={50}
        >
          <LeftControlIcon />
        </Button>
        <Button
          category={'text'}
          onPress={() => setIsPaused((isPaused) => !isPaused)}
          width={50}
        >
          {isPaused ? <PauseIcon /> : <ResumeIcon />}
        </Button>
        <Button
          category={'text'}
          onPress={() => videoRef.current.seek(100)}
          width={50}
        >
          <RightControlIcon />
        </Button>
      </Box>
    );
  };
  const ProgressBar = () => {
    const animationValue = new Animated.Value(0);

    const onEndedEvent = (event: any) => {
      setDragging(false);

      const translationX = event.nativeEvent.translationX;
      const seekTo = (translationX / (width * 0.67)) * duration + progress;
      videoRef.current.seek(seekTo);
    };

    const onDragg = (event: any) => {
      if (event.nativeEvent.state === State.ACTIVE) {
        const translationX = event.nativeEvent.translationX;

        animationValue.setValue(translationX);
      }
    };

    return (
      <Box height={16} justifyContent={'center'}>
        <Border radius={4}>
          <Box width={width * 0.7} height={8} role={'white'}>
            <Box
              width={(width * 0.7 * progress) / duration}
              height={8}
              role={'primary500'}
            />
          </Box>
        </Border>
        <PanGestureHandler
          onBegan={() => setDragging(true)}
          onEnded={(event) => onEndedEvent(event)}
          onGestureEvent={(event) => onDragg(event)}
        >
          <Animated.View
            style={[
              styles.progressBarDot,
              { transform: [{ translateX: animationValue }] },
            ]}
          />
        </PanGestureHandler>
      </Box>
    );
  };
  const ProgressBarContainer = () => {
    return (
      <Box position={'absolute'} bottom={10}>
        <Queue justifyContent={'center'} size={5}>
          <Stack>
            <Text type={'caption1'} role={'white'}>{`${getTime(
              progress
            )} / ${getTime(duration)}`}</Text>
            <ProgressBar />
          </Stack>
          <Button onPress={() => setFullScreen(true)} category={'text'}>
            <FullScreenIcon />
          </Button>
        </Queue>
      </Box>
    );
  };

  const onProgress = (data: any) => {
    if (timer === false && dragging === false) {
      setProgress(data.currentTime);
      setTimer(true);
      setTimeout(() => {
        setTimer(false);
      }, 1000);
    }
  };

  const styles = StyleSheet.create({
    touchAble: {
      width: width,
      height: height,
    },
    backgroundVideo: {
      width: width * 0.934,
      position: 'absolute',
      top: 0,
      left: width * 0.032,
      bottom: 0,
      right: 0,
      zIndex: 10,
      height: height,
    },
    progressBarDot: {
      width: 16,
      height: 16,
      position: 'absolute',
      left: (width * 0.7 * progress) / duration - 8,
      borderRadius: 8,
      borderWidth: 1,
      borderColor: '#172B4D',
      backgroundColor: 'white',
    },
  });
  return (
    <TouchableHighlight
      onPress={() => !visible && setVisible(!visible)}
      style={styles.touchAble}
    >
      <Box width={width} height={height}>
        <Video
          ref={videoRef}
          source={source}
          repeat={repeat}
          paused={isPaused}
          resizeMode={'cover'}
          fullscreen={fullScreen}
          onProgress={onProgress}
          style={styles.backgroundVideo}
          onLoad={(data) => setDuration(data.duration)}
          onFullscreenPlayerDidDismiss={() => setFullScreen(false)}
        />
        <Box
          display={visible ? 'flex' : 'none'}
          width={'100%'}
          height={'100%'}
          zIndex={11}
          opacity={0.5}
          role={'primary500'}
          position={'absolute'}
        />
        <Box
          display={visible ? 'flex' : 'none'}
          width={'93.4%'}
          height={'100%'}
          justifyContent={'center'}
          alignItems={'center'}
          zIndex={12}
          position={'absolute'}
          left={0}
        >
          <Controls />
          <ProgressBarContainer />
        </Box>
      </Box>
    </TouchableHighlight>
  );
};

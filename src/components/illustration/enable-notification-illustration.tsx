import React from 'react';
import Svg, {
  Path,
  G,
  Mask,
  Defs,
  LinearGradient,
  Stop,
} from 'react-native-svg';

export const EnableNotificationIllustration: React.FC<any> = (props) => {
  return (
    <Svg
      width={169}
      height={159}
      viewBox="0 0 169 159"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M78.362 12.167L30.984 30.806c-18.525 7.288-25.873 25.197-18.27 44.546l17.546 44.654c7.582 19.293 25.152 27.4 43.677 20.112l47.378-18.639c18.525-7.287 25.818-25.175 18.237-44.47l-17.547-44.653C114.403 13.008 96.887 4.88 78.362 12.167z"
        fill="url(#prefix__paint0_linear)"
      />
      <G filter="url(#prefix__filter0_b)">
        <Mask
          id="prefix__a"
          maskUnits="userSpaceOnUse"
          x={49.578}
          y={39.529}
          width={120}
          height={120}
          fill="#000"
        >
          <Path fill="#fff" d="M49.578 39.529h120v120h-120z" />
          <Path d="M134.77 40.53H83.867c-19.903 0-33.289 13.978-33.289 34.77v47.987c0 20.734 13.386 34.713 33.29 34.713h50.902c19.903 0 33.23-13.979 33.23-34.713V75.301c0-20.793-13.327-34.772-33.23-34.772z" />
        </Mask>
        <Path
          d="M134.77 40.53H83.867c-19.903 0-33.289 13.978-33.289 34.77v47.987c0 20.734 13.386 34.713 33.29 34.713h50.902c19.903 0 33.23-13.979 33.23-34.713V75.301c0-20.793-13.327-34.772-33.23-34.772z"
          fill="#00DCF0"
        />
        <Path
          d="M134.77 41.53a1 1 0 000-2v2zm0-2a1 1 0 100 2v-2zm0 0H83.867v2h50.903v-2zm-50.903 0c-10.194 0-18.788 3.585-24.83 9.896-6.04 6.308-9.459 15.262-9.459 25.875h2c0-10.18 3.273-18.612 8.903-24.492 5.626-5.876 13.678-9.28 23.386-9.28v-2zM49.578 75.3v47.987h2V75.301h-2zm0 47.987c0 10.585 3.42 19.524 9.459 25.824C65.08 155.414 73.673 159 83.867 159v-2c-9.709 0-17.76-3.404-23.386-9.273-5.63-5.873-8.903-14.29-8.903-24.44h-2zM83.868 159h50.902v-2H83.867v2zm50.902 0c10.194 0 18.773-3.586 24.801-9.89 6.024-6.3 9.429-15.24 9.429-25.823h-2c0 10.151-3.259 18.568-8.874 24.441-5.612 5.868-13.648 9.272-23.356 9.272v2zM169 123.287V75.301h-2v47.986h2zm0-47.986c0-10.612-3.404-19.566-9.428-25.874-6.029-6.312-14.608-9.898-24.802-9.898v2c9.708 0 17.744 3.404 23.355 9.28C163.74 56.689 167 65.118 167 75.3h2z"
          fill="url(#prefix__paint1_linear)"
          mask="url(#prefix__a)"
        />
      </G>
      <Path
        d="M112.748 118.5a4.33 4.33 0 01-7.496 0m25.415-8.667H87.333a6.499 6.499 0 006.5-6.5V92.5a15.167 15.167 0 1130.334 0v10.833a6.5 6.5 0 006.5 6.5z"
        stroke="#fff"
        strokeWidth={6}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Defs>
        <LinearGradient
          id="prefix__paint0_linear"
          x1={109.291}
          y1={0}
          x2={67.655}
          y2={120.459}
          gradientUnits="userSpaceOnUse"
        >
          <Stop stopColor="#00DCF0" />
          <Stop offset={1} stopColor="#00B4C5" />
        </LinearGradient>
        <LinearGradient
          id="prefix__paint1_linear"
          x1={69.27}
          y1={54.215}
          x2={144.21}
          y2={146.992}
          gradientUnits="userSpaceOnUse"
        >
          <Stop stopColor="#fff" stopOpacity={0.25} />
          <Stop offset={1} stopColor="#fff" stopOpacity={0} />
        </LinearGradient>
      </Defs>
    </Svg>
  );
};

import React from 'react';
import _ from 'lodash';
import { Box, Text, Shadow, Button, Spacing, Border } from '..';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { PlayIcon, LockIcon } from '../icons';
import { Image, StyleSheet } from 'react-native';

type LessonType = {
  type?: 'locked' | 'unlocked' | 'check' | 'circle';
  name?: string;
  source?: any;
  desc?: string;
  circleButton?: boolean;
  onPress: Function;
  iconType?: 'fill' | 'ghost' | 'text';
};

export const LessonDashboard: React.FC<LessonType> = ({
  type,
  name,
  source,
  desc,
  onPress,
  circleButton,
  iconType = 'fill',
}) => {
  return (
    <Shadow role={'gray'} radius={0} opacity={0.15} h={2} w={2}>
      <TouchableOpacity onPress={() => onPress()} style={styles.container}>
        <Box flex={1} flexDirection={'row'} alignItems={'center'}>
          <Box flex={1.5} height={64} width={64}>
            <Spacing ml={3}>
              <Image style={styles.image} source={source} />
            </Spacing>
          </Box>
          <Box flex={2.5} flexDirection={'column'}>
            <Text type={'callout'} fontFamily={'Montserrat'} bold>
              {name}
            </Text>
            <Text type={'caption1'} role={'primary400'}>
              {desc}
            </Text>
          </Box>
          <Box flex={1.15} justifyContent={'center'} alignItems={'center'}>
            {circleButton ? (
              <TouchableOpacity onPress={() => onPress()}>
                <Border radius={20}>
                  <Box
                    height={40}
                    width={40}
                    justifyContent="center"
                    alignItems="center"
                    role="primary500"
                  >
                    {type === 'unlocked' && <PlayIcon />}
                    {type === 'locked' && <LockIcon />}
                  </Box>
                </Border>
              </TouchableOpacity>
            ) : (
              <Button
                category={iconType}
                type={'primary'}
                width={40}
                height={40}
                onPress={() => onPress()}
              >
                {type === 'unlocked' && <PlayIcon />}
                {type === 'locked' && <LockIcon />}
              </Button>
            )}
          </Box>
        </Box>
      </TouchableOpacity>
    </Shadow>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderRadius: 8,
    width: 342,
    height: 92,
  },
  image: {
    width: 64,
    height: 64,
    borderRadius: 4,
  },
});

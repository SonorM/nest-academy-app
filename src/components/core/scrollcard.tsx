import React from 'react';
import { Image, TouchableOpacity } from 'react-native';
import { Border, Box, Spacing, Text, BackgroundImage } from '../../components';

type cardTypes = {
  source?: any;
  title?: string;
  description?: string;
  onPress?: Function;
};

export const ScrollCard: React.FC<cardTypes> = ({
  source,
  title,
  description,
  onPress,
}) => {
  return (
    <TouchableOpacity onPress={() => onPress && onPress()}>
      <Border radius={8}>
        <Box width={309} height={270} role="white">
          <Image
            source={{ uri: source }}
            style={{ width: '100%', height: 164 }}
          />
          <Spacing ph={4} pv={4}>
            <Box height={60} width="100%" justifyContent="space-evenly">
              <Text
                width="auto"
                type="subheading"
                fontFamily="Montserrat"
                role="black"
              >
                {title}
              </Text>
              <Text
                width="auto"
                type="footnote"
                fontFamily="Montserrat"
                role="primary400"
              >
                {description}
              </Text>
            </Box>
          </Spacing>
        </Box>
      </Border>
    </TouchableOpacity>
  );
};

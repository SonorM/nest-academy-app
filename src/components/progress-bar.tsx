import React, { useRef, useEffect } from 'react';
import { Box } from './layout/index';
import { Border } from './core/index';
import { Animated } from 'react-native';
import { ColorType } from '../components/types';
import { colors } from './theme-provider';

export const ProgressBar: React.FC<ProgressType> = ({
  curProgress = 0,
  endProgress = 100,
  duration = 5000,
  role = 'primary500',
  backgroundRole,
  width,
  height = '4',
}) => {
  const progress = useRef(new Animated.Value(curProgress ? curProgress : 0))
    .current;
  useEffect(() => {
    Animated.timing(progress, {
      toValue: endProgress,
      duration: duration,
      useNativeDriver: false,
    }).start();
  }, [endProgress]);

  return (
    <Box flex={1} height={4}>
      <Border radius={4}>
        <Box height={4} role={backgroundRole}>
          <Border radius={4}>
            <Animated.View
              style={{
                height: 4,
                backgroundColor: colors[role],
                width: progress.interpolate({
                  inputRange: [0, 100],
                  outputRange: ['0%', '100%'],
                }),
              }}
            ></Animated.View>
          </Border>
        </Box>
      </Border>
    </Box>
  );
};

type ProgressType = {
  width?: number | string;
  height?: number | string;
  role?: ColorType;
  backgroundRole?: ColorType;
  curProgress?: number;
  endProgress?: number;
  progress?: number;
  duration?: number;
};

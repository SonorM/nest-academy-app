import React, { useEffect, useRef } from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { TouchableOpacity, Dimensions, Animated } from 'react-native';
import { Border, Box, Text } from '../components';

const Tab = createMaterialTopTabNavigator();

export const AniTab: React.FC<any> = ({ state, descriptors, navigation }) => {
  const xValue = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.spring(xValue, {
      toValue: state.index,
      velocity: 0.1,
      useNativeDriver: true,
    }).start();
  }, [state.index]);

  return (
    <Border lineWidth={2} role={'primary500'} radius={8}>
      <Box
        height={42}
        width={342}
        flexDirection="row"
        zIndex={1}
        alignItems={'center'}
      >
        {state.routes.map((route: any, index: any) => {
          const { options } = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
              ? options.title
              : route.name;
          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true,
            });
            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name);
            }
          };

          return (
            <TouchableOpacity
              accessibilityRole="button"
              accessibilityState={isFocused ? { selected: true } : {}}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              key={route.name}
            >
              <Text
                type={'body'}
                bold
                role={isFocused ? 'white' : 'primary500'}
                textAlign="center"
              >
                {label}
              </Text>
            </TouchableOpacity>
          );
        })}
      </Box>

      {/* dood taliin guudeg animation */}
      <Box height={42} width={342} flex={1} zIndex={0} position={'absolute'}>
        <Box height={42} width={342} role="white" position={'absolute'} />
        <Animated.View
          style={[
            {
              borderRadius: 6,
              height: 42,
              width: 342 / 2,
              backgroundColor: '#172B4D',
              position: 'absolute',
            },
            {
              transform: [
                {
                  translateX: Animated.multiply(342 / 2, xValue),
                },
              ],
            },
          ]}
        />
      </Box>
    </Border>
  );
};

export const ClassToggle = () => {
  return (
    <Tab.Navigator
      swipeEnabled={false}
      style={{ width: 342 }}
      tabBar={(props) => <AniTab {...props} />}
    >
      <Tab.Screen name="1-р ээлж" component={First} />
      <Tab.Screen name="2-р ээлж" component={Second} />
    </Tab.Navigator>
  );
};

export const First = () => {
  return (
    <Box>
      <Text>First one</Text>
    </Box>
  );
};

export const Second = () => {
  return (
    <Box>
      <Text>Second one</Text>
    </Box>
  );
};

import React from 'react';
import _ from 'lodash';
import { Box, Text, Shadow, Button, Border } from '../../components';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {
  CheckIcon,
  FemaleIcon,
  FillCircleIcon,
  MaleIcon,
  NeutralIcon,
} from '../../components/icons';

type PersonalInformationGenderType = {
  type?: 'female' | 'male' | 'neutral';
  desc?: string;
  isSelected?: boolean;
};

export const PersonalInformationGender: React.FC<PersonalInformationGenderType> = ({
  type,
  desc,
  isSelected = false,
}) => {
  return (
    <Shadow role={'gray'} radius={1} opacity={0.15} h={0} w={0}>
      <Border
        radius={8}
        role={isSelected ? 'success400' : 'primary200'}
        lineWidth={1}
      >
        <Box
          role={'white'}
          width={108}
          height={123}
          alignItems={'center'}
          justifyContent={'space-evenly'}
        >
          {type === 'female' && <FemaleIcon />}
          {type === 'male' && <MaleIcon />}
          {type === 'neutral' && <NeutralIcon />}

          <Box position={'absolute'} right={10} top={10}>
            {isSelected ? (
              <CheckIcon width={20} height={20} />
            ) : (
              <FillCircleIcon />
            )}
          </Box>

          <Text type={'body'} role={'black'} width={'auto'}>
            {desc}
          </Text>
        </Box>
      </Border>
    </Shadow>
  );
};

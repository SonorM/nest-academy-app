import React, { useState } from 'react';
import { Dimensions, ScrollView } from 'react-native';
import { DropDown } from '../../components/dropdown';
import {
  Border,
  Button,
  Shadow,
  Stack,
  Queue,
  Box,
  Text,
  Spacing,
  AnimatedDynamicView,
  Avatar,
  LessonDashboard,
  BackgroundImage,
  Tag,
  Collapse,
  Lozenge,
  Input,
  InputWithHelperTextAndCounter,
  InputWithMessage,
  InputAllInOne,
  EyeIcon,
  KeyIcon,
  LoadingCircle,
  LozengeStatus,
  Banner,
  BookIcon,
  ProgressBar,
  WarnIconBG,
  PopUp,
  ExpandableText,
  ChangeAvatarImage,
  MultilineInput,
  TopBarNavigator,
  TopBarScreen,
  VideoPlayer,
  CountdownTimer,
} from '../../components';
import { CheckBoxItem, GroupCheckBox } from '../../components/group-checkbox';
import { ClassToggle } from '../../navigation/class-toggle';
import { MainTopNav } from '../../navigation/top-nav';
import { useNavigation } from '@react-navigation/native';
import { NavigationRoutes } from '../../navigation/navigation-params';

const { width } = Dimensions.get('window');
const getImage = require('../../assets/images/volu.png');

export const TestScreen = () => {
  return <MainTopNav />;
};

// test Screens
export const FirstScreen = () => {
  return (
    <Spacing p={5}>
      <ScrollView>
        <Stack size={5}>
          <VideoPlayer
            source={{
              uri:
                'https://firebasestorage.googleapis.com/v0/b/nest-academy-app.appspot.com/o/video%2FexampleVideo.mp4?alt=media&token=646101c1-5f61-4b44-81b9-8be7ace55883',
            }}
          />
          <Box flex={1} justifyContent="center">
            <Stack size={2}>
              <Text type={'title1'} bold underline>
                Typography
              </Text>
              <Text type={'title1'}>Нэг түмэн инженер</Text>
              <Text type={'title2'}>Нэг түмэн инженер</Text>
              <Text type={'title3'}>Нэг түмэн инженер</Text>
              <Text type={'headline'}>Нэг түмэн инженер</Text>
              <Text type={'body'}>Нэг түмэн инженер</Text>
              <Text type={'callout'}>Нэг түмэн инженер</Text>
              <Text type={'subheading'}>Нэг түмэн инженер</Text>
              <Text type={'caption1'}>Нэг түмэн инженер</Text>
              <Text type={'caption2'}>Нэг түмэн инженер</Text>
              <Text numberOfLines={4} type={'body'}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi
                eros dui, pellentesque eget condimentum sed, interdum non
                lectus. Proin elementum risus elit, eget tempor lorem viverra
                vitae
              </Text>
            </Stack>
          </Box>
          <Box>
            <Stack width={'100%'} size={4}>
              <Text type={'title1'} bold underline>
                ClassToggle
              </Text>
              <ClassToggle />
            </Stack>
          </Box>
          <Box>
            <Stack width={'100%'} size={4}>
              <Text type={'title1'} bold underline>
                DropDown
              </Text>
              <DropDown.Provider>
                <DropDown.Trigger title={'Боловсрол'}></DropDown.Trigger>
                {/* Olon songolt hiih bolomjtoi */}
                <DropDown.Content
                  rightIcon={true}
                  contents={['Зүйл 1', 'Зүйл 2', 'Зүйл 3', 'Зүйл 4']}
                >
                  <Text type={'body'} role={'primary500'}>
                    Зүйл 1
                  </Text>
                  <Text type={'body'} role={'primary500'}>
                    Зүйл 2
                  </Text>
                  <Text type={'body'} role={'primary500'}>
                    Зүйл 3
                  </Text>
                  <Text type={'body'} role={'primary500'}>
                    Зүйл 4
                  </Text>
                </DropDown.Content>
              </DropDown.Provider>

              {/* Neg songolt hiine */}
              <DropDown.Provider>
                <DropDown.Trigger title={'Боловсрол'}></DropDown.Trigger>

                <DropDown.Content
                  contents={['Зүйл 1', 'Зүйл 2', 'Зүйл 3', 'Зүйл 4']}
                >
                  <Text type={'body'} role={'primary500'}>
                    Зүйл 1
                  </Text>
                  <Text type={'body'} role={'primary500'}>
                    Зүйл 2
                  </Text>
                  <Text type={'body'} role={'primary500'}>
                    Зүйл 3
                  </Text>
                  <Text type={'body'} role={'primary500'}>
                    Зүйл 4
                  </Text>
                </DropDown.Content>
              </DropDown.Provider>
            </Stack>
          </Box>
          <Box flex={1}>
            <Stack width={'100%'} size={4}>
              <Text type={'title1'} bold underline>
                Buttons
              </Text>
              <Button onPress={() => console.log('handled')} size="s">
                <Text
                  fontFamily={'Montserrat'}
                  role={'white'}
                  type={'callout'}
                  textAlign={'center'}
                  bold
                >
                  Товчлуур
                </Text>
              </Button>
              <Button onPress={() => console.log('handled')} status="disabled">
                <Text
                  fontFamily={'Montserrat'}
                  role={'white'}
                  type={'callout'}
                  textAlign={'center'}
                  bold
                >
                  Товчлуур
                </Text>
              </Button>
              <Button
                onPress={() => console.log('handled')}
                status="active"
                type="destructive"
              >
                <Text
                  fontFamily={'Montserrat'}
                  role={'white'}
                  type={'callout'}
                  textAlign={'center'}
                  bold
                >
                  Товчлуур
                </Text>
              </Button>
              <Button
                onPress={() => console.log('handled')}
                category="ghost"
                type="destructive"
              >
                <Text
                  fontFamily={'Montserrat'}
                  role={'destructive500'}
                  type={'callout'}
                  textAlign={'center'}
                  bold
                >
                  Товчлуур
                </Text>
              </Button>
              <Button
                onPress={() => console.log('handled')}
                width={90}
                category="text"
              >
                <Text
                  fontFamily={'Montserrat'}
                  role={'primary500'}
                  type={'callout'}
                  textAlign={'center'}
                  bold
                >
                  Товчлуур
                </Text>
              </Button>
              <Button
                onPress={() => console.log('handled')}
                category="text"
                type="destructive"
                status="active"
                width={90}
              >
                <Text
                  fontFamily={'Montserrat'}
                  role={'destructive500'}
                  type={'callout'}
                  textAlign={'center'}
                  bold
                >
                  Товчлуур
                </Text>
              </Button>
            </Stack>
          </Box>
          <Box flex={1}>
            <Stack size={2}>
              <Text type={'title1'} bold underline>
                Spinners
              </Text>
              <Box
                flex={1}
                flexDirection={'row'}
                justifyContent={'space-evenly'}
                flexWrap={'wrap'}
              >
                <LoadingCircle width={32} height={32} />
                <LoadingCircle width={32} height={32} />
              </Box>
            </Stack>
          </Box>
        </Stack>
      </ScrollView>
    </Spacing>
  );
};

export const SecondScreen = () => {
  return (
    <Spacing p={5}>
      <ScrollView>
        <Box flex={1}>
          <Stack size={2}>
            <Text type={'title1'} bold underline>
              Countdown Timer
            </Text>
            <CountdownTimer role={'primary500'} from={3600}></CountdownTimer>
            <CountdownTimer
              countUp
              role={'primary500'}
              from={60}
            ></CountdownTimer>
          </Stack>
        </Box>

        <Stack size={5}>
          <Box flex={1} justifyContent="center">
            <Stack size={2}>
              <Text type={'title1'} bold underline>
                Inputs
              </Text>
              <Input
                placeholder={'Овог Нэр'}
                type={'default'}
                onChangeText={(text: string) => console.log(text)}
                onSubmitEditing={() => console.log('nani')}
              />
              <Input
                placeholder={'Овог Нэр'}
                type={'default'}
                LeftIcon={EyeIcon}
                onChangeText={(text: string) => console.log(text)}
                onSubmitEditing={() => console.log('nani')}
              />
              <Input
                placeholder={'Овог Нэр'}
                type={'default'}
                RigthIcon={KeyIcon}
                onChangeText={(text: string) => console.log(text)}
                onSubmitEditing={() => console.log('nani')}
              />
              <Input
                placeholder={'Овог Нэр'}
                type={'password'}
                onChangeText={(text: string) => console.log(text)}
                onSubmitEditing={() => console.log('nani')}
              />
              <InputWithHelperTextAndCounter
                placeholder={'Яаралтай үед холбоо барих'}
                counter={true}
                helperText={'Туслах текст '}
                onChangeText={(text: string) => console.log(text)}
                onSubmitEditing={() => console.log('nani')}
              />
              <InputWithMessage
                placeholder={'Овог Нэр nani'}
                messageText={'This is message'}
                messageType={'default'}
                onChangeText={(text: string) => console.log(text)}
                onSubmitEditing={() => console.log('nani')}
              />
              <InputAllInOne
                placeholder={'Овог Нэр nani'}
                messageText={'This is message'}
                messageType={'success'}
                counter={true}
                helperText={'Туслах текст '}
                onChangeText={(text: string) => console.log(text)}
                onSubmitEditing={() => console.log('nani')}
              />
              <MultilineInput
                placeholder={'TextArea'}
                type={'default'}
                onChangeText={(text: string) => console.log(text)}
                onSubmitEditing={() => console.log('test')}
                multiline={true}
              />
            </Stack>
          </Box>

          <Box flex={1}>
            <Stack size={2}>
              <Text type={'title1'} bold underline>
                Lesson Dashboard
              </Text>
              <Box flex={1} flexDirection={'column'}>
                <LessonDashboard
                  source={{
                    uri:
                      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTqwnCanQ7dgSfYKymyi1b0rx23l1YeoDacjw&usqp=CAU',
                  }}
                  type={'locked'}
                  name={'Userflow'}
                  desc={'Хичээл 2'}
                  onPress={() => console.log('pressed')}
                />
              </Box>
            </Stack>
          </Box>

          <Box flex={1}>
            <Stack size={2}>
              <Text type={'title1'} bold underline>
                Lesson Dashboard
              </Text>
              <Box flex={1} flexDirection={'column'}>
                <LessonDashboard
                  source={{
                    uri:
                      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTqwnCanQ7dgSfYKymyi1b0rx23l1YeoDacjw&usqp=CAU',
                  }}
                  type={'locked'}
                  name={'Userflow'}
                  desc={'Хичээл 2'}
                  onPress={() => console.log('pressed')}
                />
              </Box>
            </Stack>
          </Box>
          <Box flex={1}>
            <ExpandableText />
          </Box>
        </Stack>
      </ScrollView>
    </Spacing>
  );
};

export const ThirdScreen = () => {
  const [show, setShow] = useState(true);
  return (
    <Spacing p={5}>
      <ScrollView>
        <Box flex={1}>
          <Stack size={1}>
            <Text type={'title3'} bold underline>
              Collapse component
            </Text>
            <Spacing>
              <Collapse
                title={'Яаж бүртгүүлэх вэ?'}
                body={
                  'Манай бүх хөтөлбөрийн элсэлт жилийн турш цахимаар явагддаг. Та nestacademy.mn хаягаар эсвэл ЯГ одоо манай нүүр хуудаснаас swipe up хийгээд хүссэн хөтөлбөр, элсэлт боломжит улирлаа сонгон бүртгүүлэх боломжтой  '
                }
              />
            </Spacing>
          </Stack>
        </Box>
        <Stack size={5}>
          <Box flex={1}>
            <Stack size={2}>
              <Text type={'title1'} bold underline>
                Lozenge
              </Text>
              <Queue size={4}>
                <Lozenge type={'success'} style={'subtle'}>
                  АМЖИЛТТАЙ
                </Lozenge>
                <Lozenge type={'default'} style={'subtle'}>
                  DEFAULT
                </Lozenge>
              </Queue>
              <Queue size={4}>
                <Lozenge type={'pending'} style={'subtle'}>
                  ХҮЛЭЭГДЭЖ БАЙНА
                </Lozenge>
                <Lozenge type={'error'} style={'subtle'}>
                  АМЖИЛТГҮЙ
                </Lozenge>
              </Queue>
              <Queue size={4}>
                <Lozenge type={'success'} style={'bold'}>
                  АМЖИЛТТАЙ
                </Lozenge>
                <Lozenge type={'default'} style={'bold'}>
                  DEFAULT
                </Lozenge>
              </Queue>
              <Queue size={4}>
                <Lozenge type={'pending'} style={'bold'}>
                  ХҮЛЭЭГДЭЖ БАЙНА
                </Lozenge>
                <Lozenge type={'error'} style={'bold'}>
                  АМЖИЛТГҮЙ
                </Lozenge>
              </Queue>
              <Text type={'title1'} bold underline>
                Lozenge Status
              </Text>
              <LozengeStatus
                type={'default'}
                style={'subtle'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
                id={'HOPF21-0251'}
              />
              <LozengeStatus
                type={'success'}
                style={'subtle'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
                id={'HOPF21-0251'}
              />
              <LozengeStatus
                type={'pending'}
                style={'subtle'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
                id={'HOPF21-0251'}
              />
              <LozengeStatus
                type={'error'}
                style={'subtle'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
                id={'HOPF21-0251'}
              />
              <LozengeStatus
                type={'default'}
                style={'bold'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
                id={'HOPF21-0251'}
              />
              <LozengeStatus
                type={'success'}
                style={'bold'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
                id={'HOPF21-0251'}
              />
              <LozengeStatus
                type={'pending'}
                style={'bold'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
                id={'HOPF21-0251'}
              />
              <LozengeStatus
                type={'error'}
                style={'bold'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
                id={'HOPF21-0251'}
              />
              <LozengeStatus
                type={'default'}
                style={'subtle'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
              />
              <LozengeStatus
                type={'success'}
                style={'subtle'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
              />
              <LozengeStatus
                type={'pending'}
                style={'subtle'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
              />
              <LozengeStatus
                type={'error'}
                style={'subtle'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
              />
              <LozengeStatus
                type={'default'}
                style={'bold'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
              />
              <LozengeStatus
                type={'success'}
                style={'bold'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
              />
              <LozengeStatus
                type={'pending'}
                style={'bold'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
              />
              <LozengeStatus
                type={'error'}
                style={'bold'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
              />
            </Stack>
          </Box>

          <Box flex={1}>
            <Stack size={2}>
              <Text type={'title3'} bold underline>
                Dynamic animated view
              </Text>
              <Spacing ph={2}>
                <AnimatedDynamicView
                  visible={show}
                  duration={500}
                  width={'100%'}
                  height={1000}
                >
                  <Shadow
                    h={2}
                    w={0}
                    radius={4}
                    opacity={0.25}
                    role={'primary500'}
                  >
                    <Shadow
                      radius={2}
                      h={0}
                      w={0}
                      opacity={0.25}
                      role={'primary500'}
                    >
                      <Border radius={20}>
                        <Box width={'100%'} height={'auto'} role={'white'}>
                          <Spacing p={4}>
                            <Text bold type={'headline'}>
                              Dynamic animated view
                            </Text>
                            <Text type={'body'}>
                              Манай хаяг Сүхбаатар дүүрэг, Blue Sky зочид
                              буудлын чанх урд , Мэрү тауар, 504 тоот
                            </Text>
                          </Spacing>
                        </Box>
                      </Border>
                    </Shadow>
                  </Shadow>
                </AnimatedDynamicView>
              </Spacing>
              <Button
                onPress={() => setShow((show) => !show)}
                size="s"
                width={200}
              >
                <Text
                  fontFamily={'Montserrat'}
                  role={'white'}
                  type={'callout'}
                  textAlign={'center'}
                  bold
                >
                  Солих
                </Text>
              </Button>
              <Text type={'title1'} bold underline>
                Background Image
              </Text>
              <Box>
                <BackgroundImage
                  source={getImage}
                  height={164}
                  width={167}
                ></BackgroundImage>
              </Box>
            </Stack>
          </Box>

          <Box flex={1}>
            <Stack size={2}>
              <Text type={'title1'} bold underline>
                Avatars
              </Text>
              <Box
                flex={1}
                flexDirection={'row'}
                justifyContent={'space-evenly'}
                flexWrap={'wrap'}
              >
                <Avatar
                  size={'L'}
                  url={
                    'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Alesso_profile.png/467px-Alesso_profile.png'
                  }
                />
                <Avatar
                  size={'M'}
                  url={
                    'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Alesso_profile.png/467px-Alesso_profile.png'
                  }
                />
                <Avatar
                  size={'S'}
                  url={
                    'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Alesso_profile.png/467px-Alesso_profile.png'
                  }
                />
                <Avatar
                  size={'XS'}
                  url={
                    'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Alesso_profile.png/467px-Alesso_profile.png'
                  }
                />
                <Avatar size={'L'} />
                <Avatar size={'M'} />
                <Avatar size={'S'} />
              </Box>
            </Stack>
          </Box>
          <Box flex={1}>
            <GroupCheckBox defaultValue={1}>
              <CheckBoxItem checkbox index={0}>
                <Box>
                  <Spacing pt={5}>
                    <Text>This has a checkbox in the top right corner</Text>
                  </Spacing>
                </Box>
              </CheckBoxItem>
              <CheckBoxItem index={1}>
                <Box>
                  <Spacing pt={5}>
                    <Text>And this is clicked by default</Text>
                  </Spacing>
                </Box>
              </CheckBoxItem>
            </GroupCheckBox>
            <GroupCheckBox size={5} horizontal>
              <CheckBoxItem index={0}>
                <Box>
                  <Spacing pt={5}>
                    <Text>Look! A horizontal Group Checkbox</Text>
                  </Spacing>
                </Box>
              </CheckBoxItem>
              <CheckBoxItem index={1}>
                <Box>
                  <Spacing pt={5}>
                    <Text>
                      This Group Checkbox also has a customised size, by default
                      it's 4
                    </Text>
                  </Spacing>
                </Box>
              </CheckBoxItem>
            </GroupCheckBox>
          </Box>
        </Stack>
      </ScrollView>
    </Spacing>
  );
};

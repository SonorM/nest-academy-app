import React from 'react';
import { Text, Spacing, Box, BackgroundImage, Stack } from '../../components';

export const SplashSecond = () => {
  return (
    <Box flex={1} alignItems="center">
      <Spacing mt={31.5}>
        <Stack size={6} alignItems={'center'}>
          <Spacing mb={8}>
            <Box height={347}>
              <BackgroundImage source={require('../../assets/images/splash-second.png')} height={273} width={207} />
            </Box>
          </Spacing>
          <Text type={'title3'} bold textAlign={'center'} role={'primary500'}>
            Шинэ мэдлэг
        </Text>
          <Text type={'body'} textAlign={'center'} role={'gray'} width={316} bold>
            Бидний үнэгүй мини-сургалтуудыг сонирхон өөрийн мэдлэгээ
            нэмэгдүүлээрэй
        </Text>
        </Stack>
      </Spacing>
    </Box>
  );
};

import React from 'react';
import { Text, Spacing, Box, BackgroundImage, Stack } from '../../components';

export const SplashFirst = () => {
  return (
    <Box flex={1} alignItems="center">
      <Spacing mt={31.5}>
        <Stack size={6} alignItems={'center'}>
          <Spacing mb={8}>
            <Box height={347}>
              <BackgroundImage height={347} width={191} resizeMode={'contain'} source={require('../../assets/images/splash-first.png')} />
            </Box>
          </Spacing>
          <Text type={'title3'} bold textAlign={'center'} role={'primary500'}>
            Чадварлаг инженер
          </Text>
          <Text type={'body'} textAlign={'center'} role={'gray'} width={316} bold>
            Та бидэнтэй нэгдэж чадварлаг инженер болох аялалаa эхлүүлээрэй
          </Text>
        </Stack>
      </Spacing>

      <Box position={'absolute'} bottom={70}>
        <Text type={'caption2'} role={'lightgray'} bold textAlign={'center'} width={100}>SWIPE TO START</Text>
      </Box>
    </Box>
  );
};

import { useNavigation } from '@react-navigation/core';
import React, { useContext } from 'react';
import {
  FlatList,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import {
  Border,
  Box,
  Spacing,
  Text,
  BackgroundImage,
  Button,
  RightArrowIcon,
  ScrollCard,
  LessonDashboard,
  LastSeenVideo,
  Header
} from '../../components';
import { NavigationRoutes } from '../../navigation/navigation-params';
import { AuthContext } from '../../provider/auth';

const img =
  'https://proforientator.ru/publications/articles/New%20Folder/programmist_st.jpg';
const hop = require('../../assets/images/hop.png');
const leap = require('../../assets/images/leap.png');


const cardsData = [
  {
    id: 'card-0',
    title: 'DESIGN',
    description: 'UI UX design fundamentals',
    source: img,
  },
  {
    id: 'card-1',
    title: 'DESIGN',
    description: 'UI UX design fundamentals',
    source: img,
  },
  {
    id: 'card-2',
    title: 'DESIGN',
    description: 'UI UX design fundamentals',
    source: img,
  },
  {
    id: 'card-3',
    title: 'DESIGN',
    description: 'UI UX design fundamentals',
    source: img,
  },
];
const Notification = [
  {
    id: 1,
    type: 'info',
    title: 'Мэдэгдэл',
    data: 'Шинэ элсэлт дуусахад 23 цаг үлдсэн байна. Та яараарай!',
  },
  {
    id: 2,
    type: 'warning',
    title: 'Мэдэгдэл',
    data: 'Шинэ элсэлт дуусахад 23 цаг үлдсэн байна. Та яараарай!',
  },
];

export const CourseScreen = () => {
  const navigation = useNavigation();
  const { user } = useContext(AuthContext);

  return (
    <SafeAreaView>
      <Header Notification={Notification} />
      <ScrollView>
        <Spacing mt={10} />
        <Box
          width="100%"
          height={'auto'}
          justifyContent="center"
          alignItems="center"
        >
          {
            user
              ?
              <LastSeenVideo />
              :
              <Border radius={8}>
                <Box role="white" width={350} height={173}>
                  <Spacing ph={4} pv={4}>
                    <Box justifyContent="space-between" height={140}>
                      <Text
                        type="title3"
                        fontFamily="Montserrat"
                        role="primary500"
                        bold={true}
                      >
                        Илүү ихийг үз
                      </Text>
                      <Text
                        type="subheading"
                        role="primary400"
                        width={312}
                        numberOfLines={2}
                      >
                        Та манай аппликэшинд бүртгүүлснээр цааш илүү их хичээлүүдийг үзэх боломжтой болно.
                      </Text>
                      <Button width={'100%'} size='l' onPress={() => navigation.navigate(NavigationRoutes.MainCourseRegistration)}>
                        <Box flexDirection={'row'} alignItems={'center'} >
                          <Text bold role={'white'} type={'callout'} fontFamily="Montserrat">
                            БҮРТГҮҮЛЭХ
                            </Text>
                          <Spacing ml={2} />
                          <RightArrowIcon />
                        </Box>
                      </Button>
                    </Box>
                  </Spacing>
                </Box>
              </Border>
          }
        </Box>
        <Spacing mt={10} />
        <Box height="auto" width="100%" justifyContent="space-evenly">
          <Spacing ph={4}>
            <Text type="headline" role="black" bold>
              ТУРШИЛТЫН ХИЧЭЭЛ
            </Text>
            <Spacing pv={4}>
              <FlatList
                data={cardsData}
                renderItem={({ item }) => (
                  <ScrollCard
                    onPress={() => {
                      navigation.navigate(NavigationRoutes.FreeCourseScreen);
                    }}
                    source={item.source}
                    title={item.title}
                    description={item.description}
                  />
                )}
                keyExtractor={(item) => item.id}
                ItemSeparatorComponent={() => <Box width={16} />}
                showsHorizontalScrollIndicator={false}
                horizontal
              />
            </Spacing>
            <Spacing pt={4}>
              <Text type="headline" role="black" bold>
                ҮНДСЭН ХӨТӨЛБӨР
              </Text>
            </Spacing>
            <Spacing pv={4}>
              <Box
                flexDirection="row"
                width="100%"
                justifyContent="space-between"
              >
                <Border radius={8}>
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate(NavigationRoutes.HopScreen)
                    }
                  >
                    <BackgroundImage
                      source={hop}
                      height={170}
                      width={170}
                    ></BackgroundImage>
                  </TouchableOpacity>
                </Border>
                <Border radius={8}>
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate(NavigationRoutes.LeapScreen)
                    }
                  >
                    <BackgroundImage
                      source={leap}
                      height={170}
                      width={170}
                    ></BackgroundImage>
                  </TouchableOpacity>
                </Border>
              </Box>
            </Spacing>
            <Spacing pt={4}>
              <Text type="headline" role="black" bold>
                ХАМГИЙН ИХ ҮЗЭЛТТЭЙ
              </Text>
            </Spacing>
            <Box width="100%" alignItems="center">
              <Box height={445} justifyContent="center">
                <Spacing pv={4}>
                  {cardsData.map((item) => {
                    return (
                      <Spacing mv={2} key={item.id}>
                        <LessonDashboard
                          source={{ uri: item.source }}
                          name={item.title}
                          desc={item.description}
                          onPress={() => console.log('ahahaha')}
                          type="unlocked"
                          circleButton
                        />
                      </Spacing>
                    );
                  })}
                </Spacing>
              </Box>
              <Spacing pv={4}>
                <Button
                  onPress={() => {
                    console.log('aajjajaja');
                  }}
                  size="s"
                  width={343}
                >
                  <Box
                    flexDirection="row"
                    justifyContent="center"
                    alignItems="center"
                  >
                    <Box>
                      <Text
                        type="callout"
                        role="white"
                        bold={true}
                        fontFamily="Montserrat"
                      >
                        Цааш үзэх
                      </Text>
                    </Box>
                    <Box width={40} justifyContent="center" alignItems="center">
                      <RightArrowIcon />
                    </Box>
                  </Box>
                </Button>
              </Spacing>
            </Box>
          </Spacing>
          <Box height={120} />
        </Box>
      </ScrollView>
    </SafeAreaView>
  );
};

import React from 'react';
import { Image, TouchableOpacity } from 'react-native';
import { Border, Box, Spacing, Text } from '../../components';
import { useNavigation } from '@react-navigation/native';
import { NavigationRoutes } from '../../navigation/navigation-params';

interface Props {
  source?: any;
  title?: string;
  date?: string;
  screenName?: string;
}

export const NewsCard: React.FC<Props> = ({
  source,
  title,
  date,
  screenName,
}) => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate(NavigationRoutes[screenName]);
      }}
    >
      <Border radius={8}>
        <Box width={309} height={270} role={'primary100'}>
          <Image style={{ width: '100%', height: 164 }} source={source} />
          <Spacing pl={3.5}>
            <Box height={106} width={'100%'} justifyContent={'space-evenly'}>
              <Text width={'auto'} type={'body'} fontFamily={'Montserrat'} bold>
                {title}
              </Text>
              <Text
                width={'auto'}
                type={'subheading'}
                fontFamily={'Montserrat'}
              >
                {date}
              </Text>
            </Box>
          </Spacing>
        </Box>
      </Border>
    </TouchableOpacity>
  );
};

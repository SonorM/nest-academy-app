import React from 'react';
import { FlatList, SafeAreaView } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { PhotoCard } from './photo-card';
import {
  Text,
  Spacing,
  HopImage,
  LeapImage,
  Box,
  ClockIcon,
  ThumbIcon,
  PriceIcon,
  Stack,
  Queue,
  ArrowIcon,
  Border,
  DesignIcon,
  Button,
  ExpandableText,
  TopBarNavigator,
  TopBarScreen,
  LoadingCircle,
  RightArrIcon,
  PrimaryRightArrowIcon
} from '../../components';
import { ClassToggle } from '../../navigation/class-toggle';
import { AdmissionProcess } from './admission-process';
import { NavigationRoutes } from '../../navigation/navigation-params';
const duuk = require('../../assets/images/duuk-bagsh.png');
import { useQuery } from '@apollo/client';
import { REQUEST_HOP_SCREEN } from '../../graphql/queries';
import { REQUEST_LEAP_SCREEN } from '../../graphql/queries';
import _ from 'lodash';
import { CurriculmScreen } from '../curriculum';

export const LeapScreen: React.FC<any> = ({ tabs, y }) => {
  const navigation = useNavigation();

  const Header = () => {
    return (
      <Box
        role="primary500"
        width={'100%'}
        height={280}
        justifyContent={'center'}
        alignItems={'center'}
      >
        <LeapImage />
      </Box>
    );
  };

  return (
    <Box>
      <TopBarNavigator Header={Header}>
        <TopBarScreen name="Тухай" component={LeapAboutScreen} />
        <TopBarScreen name="Хөтөлбөр" component={CurriculmScreen} />
        {/* <TopBarScreen name="Бүтээлүүд" component={WorksScreen} /> */}
      </TopBarNavigator>
      <Box bottom={0} position={'absolute'} width={'100%'} height = {90} role = {'white'}>
        <Spacing pt = {1} pl = {4} pr = {4}>
        <Button
          size={'l'}
          width={'100%'}
          onPress={() => navigation.navigate(NavigationRoutes.Registration)}
        >
          <Text fontFamily={'Montserrat'} type={'callout'} role={'white'} bold>
            БҮРТГҮҮЛЭХ
          </Text>
        </Button>
        </Spacing>
      </Box>
    </Box>
  );
};
export const LeapAboutScreen = () => {
  const navigation = useNavigation();
  const { data, error, loading } = useQuery(REQUEST_LEAP_SCREEN);

  if (loading) {
    return (
      <SafeAreaView>
        <Box flex={1} alignItems={'center'} justifyContent={'center'}>
          <LoadingCircle />
        </Box>
      </SafeAreaView>
    );
  }

  return (
    <Box>
      <Spacing pt={9} pl = {5} pb = {5}>
        <Box>
          <Text fontFamily={'Montserrat'} bold type={'body'}>
            {data && data.leapAboutCollection.items[0].leapAboutAdvantagesText}
          </Text>
          <Spacing mt={8}>
            <Stack size={6}>
              <Box
                flexDirection={'row'}
                flex={1}
                justifyContent={'space-between'}
                alignItems={'center'}
              >
                <ThumbIcon />
                <Box width={'auto'}>
                  <Text bold type={'body'} numberOfLines={3}>
                    Чадварлаг инженер
                  </Text>
                  <Text numberOfLines={3} width={263}>
                    {data && data.leapAdvantageCollection.items[7].advantage}
                  </Text>
                </Box>
              </Box>
              <Box
                flexDirection={'row'}
                flex={1}
                justifyContent={'space-between'}
                alignItems={'center'}
              >
                <PriceIcon />
                <Box width={'auto'}>
                  <Text bold type={'body'} numberOfLines={3}>
                    Боломжийн үнэ
                  </Text>
                  <Text numberOfLines={3} width={263}>
                    {data && data.leapAdvantageCollection.items[1].advantage}
                  </Text>
                </Box>
              </Box>
              <Box
                flexDirection={'row'}
                flex={1}
                justifyContent={'space-between'}
                alignItems={'center'}
              >
                <ClockIcon />
                <Box width={'auto'}>
                  <Text bold type={'body'} numberOfLines={3}>
                    Цаг хэмнэх
                  </Text>
                  <Text numberOfLines={3} width={263}>
                    {data && data.leapAdvantageCollection.items[3].advantage}
                  </Text>
                </Box>
              </Box>
            </Stack>
          </Spacing>
        </Box>
        <Spacing mt={6} mb={6}>
          <Border lineWidth={1} role="primary200" />
        </Spacing>
        <ExpandableText />
        <Spacing mt={6} mb={6}>
          <Border lineWidth={1} role="primary200" />
        </Spacing>
        <Box>
          <Text fontFamily={'Montserrat'} bold type={'body'}>
            Цаг хувиарлалт
          </Text>
          <Spacing mt={4}>
            <Box>
              <ClassToggle />
            </Box>
          </Spacing>
        </Box>
        <Spacing mt={6} mb={6}>
          <Border lineWidth={1} role="primary200" />
        </Spacing>
        <Box>
          <FlatList
            data={data}
            renderItem={({ item }) => (
              <PhotoCard
                source={item.source}
                title={item.title}
                name={item.name}
                width={264}
                height={320}
                icon={<DesignIcon />}
              />
            )}
            keyExtractor={(item) => item.id}
            ItemSeparatorComponent={() => <Box width={16} />}
            showsHorizontalScrollIndicator={false}
            horizontal
          />
          <Text bold type={'body'}>
            Дэлгэрэнгүй
          </Text>
          <Spacing mt={2} />
          <Text>{data && data.leapAboutCollection.items[0].title}</Text>
        </Box>
        <Spacing mt={6} mb={6}>
          <Border lineWidth={1} role="primary200" />
        </Spacing>
        <Box>
          <Text fontFamily={'Montserrat'} bold type={'body'}>
            Элсэлтийн явц
          </Text>
          <Spacing m={4} />
          <AdmissionProcess />
        </Box>
      </Spacing>
      <Spacing>
        <Border topWidth={1} bottomWidth={1} role="primary300">
          <Box height={56} justifyContent="center">
            <Spacing mh={5}>
              <Queue justifyContent="space-between">
                <Queue>
                  <Spacing>
                    <Text fontFamily={'Montserrat'} type="body" role="black">
                      FAQ
                    </Text>
                  </Spacing>
                </Queue>
                <Box position="absolute" right={5} bottom={-16}>
                  <Button
                    category="text"
                    status="active"
                    height={64}
                    onPress={() =>
                      navigation.navigate(NavigationRoutes.FaqScreen)
                    }
                  >
                    <PrimaryRightArrowIcon width = {24} height = {24}/>
                  </Button>
                </Box>
              </Queue>
            </Spacing>
          </Box>
        </Border>
      </Spacing>
      <Border bottomWidth={1} role="primary300">
        <Box height={56} justifyContent="center">
          <Spacing mh={5}>
            <Queue justifyContent="space-between">
              <Queue>
                <Spacing>
                  <Text fontFamily={'Montserrat'} type="body" role="black">
                    Contact
                  </Text>
                </Spacing>
              </Queue>
              <Box position="absolute" right={5} bottom={-16}>
                <Button
                  category="text"
                  status="active"
                  height={64}
                  onPress={() => navigation.navigate('')}
                >
                    <PrimaryRightArrowIcon width = {24} height = {24}/>
                </Button>
              </Box>
            </Queue>
          </Spacing>
        </Box>
      </Border>
      <Spacing m={15} />
    </Box>
  );
};

export const LeapProgramScreen = () => {
  return (
    <SafeAreaView>
      <Text></Text>
    </SafeAreaView>
  );
}

export const HopScreen: React.FC<any> = ({ tabs, y }) => {
  const navigation = useNavigation();
  const {data, error, loading} = useQuery(REQUEST_HOP_SCREEN)
  const ExpectationText = data?.course?.expectationText?.json?.content[0].content;
  const PromotionText = data?.course?.promotionText?.json?.content[0].content;
  const StagesOfCourse = data?.course?.stagesOfCourseCollection?.items;
  const StagesOfCourseInfo = data?.course?.stagesOfCourseCollection?.items[0].description?.json?.content[0].content; 

  if (loading) {
    return (
      <Box flex={1} alignItems={'center'} justifyContent={'center'}>
        <LoadingCircle />
      </Box>
    );
  }

  const Header = () => {
    return (
      <Box
        role="caution500"
        width={'100%'}
        height={280}
        justifyContent={'center'}
        alignItems={'center'}
      >
        <HopImage />
      </Box>
    );
  };

  return (
    <Box>
      <TopBarNavigator Header={Header}>
        <TopBarScreen name="Тухай" component={HopAboutScreen} />
        <TopBarScreen name="Хөтөлбөр" component={CurriculmScreen} />
        {/* <TopBarScreen name="Бүтээлүүд" component={WorksScreen} /> */}
      </TopBarNavigator>
      <Box bottom={0} position={'absolute'} width={'100%'} height = {90} role = {'white'}>
        <Spacing pt = {1} pl = {4} pr = {4}>
        <Button
          size={'l'}
          width={'100%'}
          onPress={() => navigation.navigate(NavigationRoutes.Registration)}
        >
          <Text fontFamily={'Montserrat'} type={'callout'} role={'white'} bold>
            БҮРТГҮҮЛЭХ
          </Text>
        </Button>
        </Spacing>
      </Box>
    </Box>
  );
};

export const HopAboutScreen = () => {
  const navigation = useNavigation();
  const { data, error, loading } = useQuery(REQUEST_HOP_SCREEN);
  const Teachers = data?.course?.teachersCollection?.items;

  if (loading) {
    return (
      <Box flex={1} alignItems={'center'} justifyContent={'center'}>
        <LoadingCircle />
      </Box>
    );
  }

  return (
    <Box>
      <Spacing pt={9} pl = {5} pb = {5}>
        <Box>
          <Text fontFamily={'Montserrat'} bold type={'body'}>
            Давуу талууд
          </Text>
          <Spacing mt={8}>
            <Stack size={6}>
              <Box
                flexDirection={'row'}
                flex={1}
                justifyContent={'space-between'}
                alignItems={'center'}
              >
                <ThumbIcon />
                <Box width={'auto'}>
                  <Text bold type={'body'} numberOfLines={3}>
                    Чадварлаг инженер
                  </Text>
                  <Text numberOfLines={3} width={263}>
                    {/* {data && data.leapAdvantageCollection.items[7].advantage} */}
                  </Text>
                </Box>
              </Box>
              <Box
                flexDirection={'row'}
                flex={1}
                justifyContent={'space-between'}
                alignItems={'center'}
              >
                <PriceIcon />
                <Box width={'auto'}>
                  <Text bold type={'body'} numberOfLines={3}>
                    Боломжийн үнэ
                  </Text>
                  <Text numberOfLines={3} width={263}>
                    {/* {data && data.leapAdvantageCollection.items[1].advantage} */}
                  </Text>
                </Box>
              </Box>
              <Box
                flexDirection={'row'}
                flex={1}
                justifyContent={'space-between'}
                alignItems={'center'}
              >
                <ClockIcon />
                <Box width={'auto'}>
                  <Text bold type={'body'} numberOfLines={3}>
                    Цаг хэмнэх
                  </Text>
                  <Text numberOfLines={3} width={263}>
                    {/* {data && data.leapAdvantageCollection.items[3].advantage} */}
                  </Text>
                </Box>
              </Box>
            </Stack>
          </Spacing>
        </Box>
        <Spacing mt={6} mb={6}>
          <Border lineWidth={1} role="primary200" />
        </Spacing>
        <ExpandableText />
        <Spacing mt={6} mb={6}>
          <Border lineWidth={1} role="primary200" />
        </Spacing>
        <Box>
          <Text fontFamily={'Montserrat'} bold type={'body'}>
            Цаг хувиарлалт
          </Text>
          <Spacing mt={4}>
            <Box>
              <ClassToggle />
            </Box>
          </Spacing>
        </Box>
        <Spacing mt={6} mb={6}>
          <Border lineWidth={1} role="primary200" />
        </Spacing>
        <Box>
          <FlatList
            data={Teachers}
            renderItem={({ item }) => (
              <PhotoCard
                ratio={0.9}
                source={{ uri: item.picture.url }}
                title={item.class}
                name={item.name}
                width={264}
                height={320}
                icon={<DesignIcon />}
              />
            )}
            keyExtractor={(item) => item.id}
            ItemSeparatorComponent={() => <Box width={16} />}
            showsHorizontalScrollIndicator={false}
            horizontal
          />
          <Spacing mt={6} />
          <Text bold type={'body'}>
            Дэлгэрэнгүй
          </Text>
          <Spacing mt={2} />
          <Text numberOfLines = {4}>
          Энэ чиглэлээр 4-өөс дээш жил ажилласан туршлагатай, Вэб, Мобайл програмын дизайн, хөгжүүлэлтэд анхаарч ажилладаг бүтээгдэхүүний дизайнер
          </Text>
        </Box>
        <Spacing mt={6} mb={6}>
          <Border lineWidth={1} role="primary200" />
        </Spacing>
        <Box>
          <Text fontFamily={'Montserrat'} bold type={'body'}>
            Элсэлтийн явц
          </Text>
          <Spacing m={4} />
          <AdmissionProcess />
        </Box>
      </Spacing>
      <Spacing>
        <Border topWidth={1} bottomWidth={1} role="primary300">
          <Box height={56} justifyContent="center">
            <Spacing mh={5}>
              <Queue justifyContent="space-between">
                <Queue>
                  <Spacing>
                    <Text fontFamily={'Montserrat'} type="body" role="black">
                      FAQ
                    </Text>
                  </Spacing>
                </Queue>
                <Box position="absolute" right={5} bottom={-16}>
                  <Button
                    category="text"
                    status="active"
                    height={64}
                    onPress={() =>
                      navigation.navigate(NavigationRoutes.FaqScreen)
                    }
                  >
                    <PrimaryRightArrowIcon width = {24} height = {24}/>
                  </Button>
                </Box>
              </Queue>
            </Spacing>
          </Box>
        </Border>
      </Spacing>
      <Border bottomWidth={1} role="primary300">
        <Box height={56} justifyContent="center">
          <Spacing mh={5}>
            <Queue justifyContent="space-between">
              <Queue>
                <Spacing>
                  <Text fontFamily={'Montserrat'} type="body" role="black">
                    Contact
                  </Text>
                </Spacing>
              </Queue>
              <Box position="absolute" right={5} bottom={-16}>
                <Button
                  category="text"
                  status="active"
                  height={64}
                  onPress={() => navigation.navigate('')}
                >
                    <PrimaryRightArrowIcon width = {24} height = {24}/>
                </Button>     
              </Box>
            </Queue>
          </Spacing>
        </Box>
      </Border>
      <Spacing m={15} />
    </Box>
  );
};

export const HopProgramScreen = () => {
  return <Text></Text>;
};

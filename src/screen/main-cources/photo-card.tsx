import React from 'react';
import {
  BackgroundImage,
  Border,
  Box,
  Queue,
  Spacing,
  Text,
} from '../../components';

interface Texts {
  name?: string;
  icon?: any;
  title?: string;
  source?: any;
  width?: number | string;
  height?: number | string;
  ratio?: number;
}

export const PhotoCard: React.FC<Texts> = ({
  name,
  title,
  source,
  width,
  height,
  icon,
  ratio = 1
}) => {
  const Icon = () => {
    return <>{icon}</>;
  };
  // console.log('URI', source);

  return (
    <Border radius={4} role={'accentNest'}>
      <Box></Box>
      <BackgroundImage
        ratio={ratio}
        opacity={0.8}
        role={'black'}
        resizeMode={'cover'}
        source={source}
        width={width}
        height={height}
      >
        <Box justifyContent={'flex-end'} height={'100%'} width={'100%'}>
          <Spacing p={5}>
            <Text
              width={'auto'}
              role={'white'}
              type={'body'}
              fontFamily={'Montserrat'}
              bold
            >
              {name}
            </Text>
            <Spacing pt = {2}>
            <Queue>
              <Icon />
              <Text
                width={'auto'}
                role={'accentNest'}
                type={'body'}
                fontFamily={'Montserrat'}
                bold
              >
                {title}
              </Text>
            </Queue>
            </Spacing>
          </Spacing>
        </Box>
      </BackgroundImage>
    </Border>
  );
};

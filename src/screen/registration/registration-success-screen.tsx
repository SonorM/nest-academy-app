import React, { useContext } from 'react';
import { SafeAreaView, TouchableOpacity, Share } from 'react-native';
import { Box, Queue, Spacing, Stack } from '../../components/layout';
import { CloseLineIcon, ShareIcon } from '../../components/icons';
import {
  AdmissionProcessCard,
  AdmissionProcessCardContent,
  AdmissionProcessCardHeader,
  Button,
  CheckCircleIllustration,
  Text,
} from '../../components';
import { useNavigation } from '@react-navigation/native';
import { NavigationRoutes } from '../../navigation/navigation-params';
import { useDocument } from '../../hooks';
import { AuthContext } from '../../provider/auth';

export const RegistrationSuccessScreen: React.FC<RegistrationSuccessScreenType> = ({ route }) => {
  const { admissionId } = route.params || {};
  const navigation = useNavigation();
  let { user } = useContext(AuthContext);
  let { uid } = user || {};
  const { doc: admissionData }: any = useDocument(`users/${uid}/admissions/${admissionId}`);

  const shareExamLink = async () => {
    try {
      const result = await Share.share({
        message: 'Generate dynamic link soon...',
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Box>
        <Spacing m={3}>
          <Queue justifyContent={'flex-end'}>
            <TouchableOpacity
              onPress={() => navigation.navigate(NavigationRoutes.MainRoot)}
            >
              <CloseLineIcon width={20} height={20}></CloseLineIcon>
            </TouchableOpacity>
          </Queue>
          <Spacing mt={6} ml={7} mr={7}>
            <Stack alignItems={'center'} size={2}>
              <CheckCircleIllustration />
              <Spacing mt={2}/>
              <Text
                textAlign={'center'}
                bold
                type={'title3'}
                fontFamily={'Montserrat'}
              >
                Бүртгэл амжилттай
              </Text>
              <Text textAlign={'center'} type={'body'}>
                Та {admissionData && admissionData?.course == 'leap' ? 'LEAP' : 'HOP'} хөтөлбөрт амжилттай бүртгүүллээ
              </Text>
            </Stack>
          </Spacing>
        </Spacing>
      </Box>
      <Spacing m={4}>
        <Stack size={4}>
          <AdmissionProcessCard>
            <AdmissionProcessCardHeader>
              <Spacing ph={4} pv={3}>
                <Text bold type={'headline'} role={'black100'}>
                  Шалгалт
                </Text>
              </Spacing>
            </AdmissionProcessCardHeader>
            <AdmissionProcessCardContent>
              <Spacing m={4}>
                <Stack size={4}>
                  <Text type={'footnote'}>
                    Та шалгалтаа одоо өгч болно. Шалгалтаа одоо өгөх дараа өгөх
                    ямар ч ялгаа байхгүй
                  </Text>
                  <Button
                    onPress={() =>
                      navigation.navigate(NavigationRoutes.ExamStack)
                    }
                    width={'100%'}
                  >
                    <Text
                      type={'headline'}
                      role={'primary100'}
                      bold
                      fontFamily={'Montserrat'}
                    >
                      эхлэх
                    </Text>
                  </Button>
                </Stack>
              </Spacing>
            </AdmissionProcessCardContent>
          </AdmissionProcessCard>
          <AdmissionProcessCard>
            <AdmissionProcessCardHeader>
              <Spacing ph={4} pv={3}>
                <Text bold type={'headline'}  role={'black100'}>
                  Холбоос дамжуулах
                </Text>
              </Spacing>
            </AdmissionProcessCardHeader>
            <AdmissionProcessCardContent>
              <Spacing m={4}>
                <Stack size={4}>
                  <Text type={'footnote'}>
                    Элсэгч хаанаас ч энэ холбоосийг ашиглаж шалгалтаа өгч болно
                  </Text>
                  <Button
                    onPress={shareExamLink}
                    category={'ghost'}
                    width={'100%'}
                  >
                    <Queue alignItems={'center'}>
                      <ShareIcon></ShareIcon>
                      <Spacing ml={1} />
                      <Text
                        type={'headline'}
                        role={'primary500'}
                        bold
                        fontFamily={'Montserrat'}
                      >
                        ХУВААЛЦАХ
                      </Text>
                    </Queue>
                  </Button>
                </Stack>
              </Spacing>
            </AdmissionProcessCardContent>
          </AdmissionProcessCard>
        </Stack>
      </Spacing>
    </SafeAreaView>
  );
};

type RegistrationSuccessScreenType = {
  route: any;
};

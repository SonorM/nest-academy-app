import React, { useState, useContext } from 'react';
import { Image } from 'react-native';
import {
  AdmissionProcessCard,
  AdmissionProcessCardContent,
  AdmissionProcessCardHeader,
} from '../../components/admission-process-card';
import { Text, Border } from '../../components/core';
import {
  Spacing,
  Box,
  BackgroundImage,
  Queue,
} from '../../components/layout';
import { InputWithHelperTextAndCounter } from '../../components/inputs';
import { CheckBoxItem, GroupCheckBox } from '../../components/group-checkbox';
import { ExamContext } from '../../provider/exam-provider';

import _ from 'lodash';

export const ExamCard: React.FC<any> = (props) => {
  const { setScore, score } = useContext(ExamContext);
  const ans = ['A', 'B', 'C', 'D', 'E'];
  const [isAnswered, setIsAnswered] = useState(false);
  const [inputText, setInputText] = useState('');
  const [correct, setCorrect] = useState(false);
  const {
    type,
    imageSrc = null,
    question = 'Дөрвөн дүрсний аль гурав нь нийлж нэг дөрвөлжин үүсгэх вэ?',
    correctAns,
    answer,
    qId = 1,
  } = props;

  const Answered = (element: any) => {
    if (element == correctAns) {
      setCorrect(true);
      setScore(score + 1);
    } else if (correct == true) {
      setScore(score - 1);
      setCorrect(false);
    }
    setIsAnswered(true);
    return;
  };

  return (
    <Box>
    <AdmissionProcessCard >
      <AdmissionProcessCardHeader>
        {isAnswered ? (
          <Spacing p={0} >
          <Box role={'primary200'} flexDirection={'column'}>
            <Spacing pt={3} pb={3} pl={3} pr={3}>
              <Spacing pb={3}>
                <Box width={'100%'}>
                  <Queue justifyContent={'space-between'}>
                    <Text fontFamily={'Montserrat'} type={'caption1'}>
                      Асуулт №{qId}
                    </Text>
                    <Text
                      fontFamily={'Montserrat'}
                      type={'caption1'}
                      role={'primary500'}
                      bold
                    >
                      Хариулсан
                    </Text>
                  </Queue>
                </Box>
              </Spacing>
              <Text fontFamily={'Montserrat'} role={'primary500'} type={'body'} bold>
                {question}
              </Text>
            </Spacing>
          </Box>
          </Spacing>
        ) : (
          <Box flexDirection={'column'}>
            <Spacing pt={3} pb={3} pl={3} pr={3}>
              <Spacing pb={3}>
                <Box width={'100%'}>
                  <Queue justifyContent={'space-between'}>
                    <Text fontFamily={'Montserrat'} role={'gray'} bold type={'caption1'}>
                      Асуулт №{qId}
                    </Text>
                    <Border radius={4}>
                      <Box
                        role={'caution200'}
                        height={24}
                        width={96}
                        justifyContent={'center'}
                        alignItems={'center'}
                      >
                        <Text
                          fontFamily={'Montserrat'}
                          type={'caption1'}
                          role={'caution500'}
                          textAlign={'center'}
                          bold
                        >
                          Хариулаагүй
                        </Text>
                      </Box>
                    </Border>
                  </Queue>
                </Box>
              </Spacing>
              <Text fontFamily={'Montserrat'} role={'primary500'} type={'body'} bold>
                {question}
              </Text>
            </Spacing>
          </Box>
        )}
        {(type == 'graphicquiz' || type == 'graphicquizwithimage') && (
          <Box alignItems={'center'}>
            <Image
              style={{ width: 311, height: 242, resizeMode: 'contain' }}
              source={{ uri: imageSrc }}
            />
          </Box>
        )}
      </AdmissionProcessCardHeader>
      <AdmissionProcessCardContent >
        {type == 'shortanswerquiz' ? (
          <Spacing mt={4} mb={4} ml={4} mr={4}>
            <Box justifyContent={'center'} alignItems={'center'}>
              <InputWithHelperTextAndCounter
                placeholder={'Хариулт'}
                helperText={'Та дээрх хэсэгт өөрийн хариултаа бичнэ үү.'}
                onChangeText={(text: string) => setInputText(text)}
                onSubmitEditing={() => Answered(inputText)}
              />
            </Box>
          </Spacing>
        ) : (
          <>
            <Spacing mt={4} mb={4} ml={4} mr={4}>
              {type == 'graphicquizwithimage' ? (
                <Box
                  flexWrap={'wrap'}
                  height={'auto'}
                  width={'100%'}
                  flexDirection={'row'}
                  justifyContent={'space-around'}
                >
                  {_.isArray(answer) ? (
                    answer.map((e, i) => {
                      return (
                        <Box flexDirection={'column'}>
                          <Spacing mb={2} >
                            <Text bold>{ans[i]}</Text>
                          </Spacing>
                          <Image
                            style={{
                              width: 120,
                              height: 120,
                              resizeMode: 'contain',
                            }}
                            source={{ uri: e }}
                          />
                        </Box>
                      );
                    })
                  ) : (
                    <Box flexDirection={'column'}>
                      <BackgroundImage
                        width={'100%'}
                        source={{ uri: answer }}
                        resizeMode={'contain'}
                        ratio={1}
                      ></BackgroundImage>
                    </Box>
                  )}
                </Box>
              ) : (
                <Box height={'auto'} width={'100%'} alignSelf={'flex-start'}>
                  {answer.map((e: any, i: any) => {
                    return (
                      <Box flexDirection={'row'} width={'100%'} key={i}>
                        <Spacing mb={2} >
                          <Text bold>{(ans[i]) + ') '}</Text>
                        </Spacing>
                        <Text>{e}</Text>
                      </Box>
                    );
                  })}
                </Box>
              )}
            </Spacing>
            <Spacing ml={4} mr={4} mb={3}>
              <Box
                height={60}
                flexDirection={'row'}
                justifyContent={'space-between'}
              >
                <GroupCheckBox size={6}>
                  {_.isArray(answer)
                    ? _.map(answer, (e, i) => {
                        return (
                          <CheckBoxItem
                            size={[12, 8, 12]}
                            checkbox
                            index={i}
                            onPress={() => {
                              type == 'graphicquizwithimage'
                                ? Answered(ans[i][0])
                                : Answered(answer[i]);
                            }}
                            onUnPress={() => {
                              console.log('pressed');
                            }}
                          >
                            <Box width={56} height={56}>
                              <Spacing m={3}>
                                <Box
                                  height="100%"
                                  justifyContent="center"
                                  alignItems="center"
                                >
                                  <Text type={'callout'} bold>
                                    {(ans[i])}
                                  </Text>
                                </Box>
                              </Spacing>
                            </Box>
                          </CheckBoxItem>
                        );
                      })
                    : _.map(ans, (e, i) => {
                        return (
                          <CheckBoxItem
                            size={[12, 8, 12]}
                            checkbox
                            index={i}
                            onPress={() => {
                              type == 'graphicquizwithimage'
                                ? Answered(ans[i][0])
                                : Answered(answer[i]);
                            }}
                            onUnPress={() => {
                              console.log('pressed');
                            }}
                          >
                            <Box width={56} height={56}>
                              <Spacing m={3}>
                                <Box
                                  height="100%"
                                  justifyContent="center"
                                  alignItems="center"
                                >
                                  <Text type={'callout'} bold>
                                    {(ans[i])}
                                  </Text>
                                </Box>
                              </Spacing>
                            </Box>
                          </CheckBoxItem>
                        );
                      })}
                </GroupCheckBox>
              </Box>
            </Spacing>
          </>
        )}
      </AdmissionProcessCardContent>
    </AdmissionProcessCard>
    </Box>

  );
};

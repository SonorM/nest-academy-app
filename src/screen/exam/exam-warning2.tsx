import React from 'react';
import {
  Box,
  Warning,
  TimeLineIllustration,
  TopNavigation,
  Text,
} from '../../components';
import { useNavigation } from '@react-navigation/native';

export const ExamWarningScreen2 = () => {
  const navigation = useNavigation();
  return (
    <Box
      flex={1}
      alignItems="center"
      justifyContent="space-evenly"
      role="white"
    >
      <TimeLineIllustration />
      <Warning
        onPress={() => {
          navigation.navigate('ExamWarning3');
        }}
        title="Санамж #2"
        buttonname="Дараах"
      >
        <Text role="primary500" type="body" textAlign="center" width={275}>
          Та шалгалтаас гарсан тохиолдолд таны оноо гүйцэтгэсэн ажлаар дүгнэгдэх
          болно.
        </Text>
      </Warning>
    </Box>
  );
};

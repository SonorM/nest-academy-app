import { NavigationRouteContext, useNavigation } from '@react-navigation/native';
import React, { createRef, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { Text, Button, Stack, Box, Border, FailBirdIllustration, AdmissionProcessCard, AdmissionProcessCardContent, AdmissionProcessCardHeader, ExamSuccess, Queue, DocumentIcon, Shadow, Spacing } from '../../components'
import { NavigationRoutes } from '../../navigation/navigation-params';
import { HomeScreen } from '../home';

export const ExamResultScreen: React.FC<any> = ({ route }) => {
    const { score, totalScore } = route.params || {};
    const navigation = useNavigation();

    const percent = (score / totalScore) * 100;
    return (
        <Box flex={1} alignItems={'center'} role={'fawhite'}>
            <Box position={'relative'} flex={7} width={'100%'} alignItems={'center'} justifyContent={'center'}>
                {percent >= 60 ? (
                    <Box position={'relative'} width={340} height={'auto'} alignItems={'center'} justifyContent={'center'}>
                        <Spacing mt={20} mb={5}>
                            <ExamSuccess />
                        </Spacing>
                        <Stack size={6} alignItems={'center'}>
                            <Text bold textAlign={'center'} type={'title3'} role={'primary500'}>Амжилттай тэнцлээ!</Text>
                            <Text width={250} role={'black'} textAlign={'center'}>Та элсэлтийн шалгалтанд амжилттай тэнцлээ.</Text>
                            <Spacing mh={-1} m={3}>
                            <AdmissionProcessCard>
                                <AdmissionProcessCardHeader>
                                    <Spacing p={3} >
                                        <Text bold type={'headline'}>Ярилцлага</Text>
                                    </Spacing>
                                </AdmissionProcessCardHeader>
                                <AdmissionProcessCardContent>
                                    <Spacing m={3}>
                                        <Stack size={4}>
                                            <Text type={'footnote'}>Та манай элсэлтэнд бүрэн хамрагдахын тулд ярилцлага товлох шаардлагатай. Та цагаа товлоно уу.</Text>
                                            <Button
                                                onPress={() =>
                                                    navigation.navigate(NavigationRoutes.BookMeeting)
                                                }
                                                width={'100%'}
                                            ><Text
                                                type={'headline'}
                                                role={'primary100'}
                                                bold
                                                fontFamily={'Montserrat'}>Цаг товлох</Text>
                                            </Button>
                                        </Stack>
                                    </Spacing>
                                </AdmissionProcessCardContent>
                            </AdmissionProcessCard>
                            </Spacing>
                        </Stack>
                    </Box>
                ) : (
                    <Box position={'relative'} width={'auto'} height={'auto'} alignItems={'center'} justifyContent={'center'}>
                        <Spacing mb={9}>
                            <FailBirdIllustration />
                        </Spacing>
                        <Stack size={6}>
                            <Text textAlign={'center'} role={'primary500'} type={'title3'} bold>Уучлаарай та тэнцсэнгүй</Text>
                            <Text width={320} textAlign={'center'} role={'black'} type={'body'}>Та хүлээлгийн эгнээнд шилжиж байна. Дараачийн элсэлтээр шалгалт дахин өгөх боломжтой.</Text>
                            <AdmissionProcessCard>
                                <Border radius={4}>
                                    <Box width={343} alignSelf={'center'} height={97} justifyContent={'center'} alignItems={'center'}>
                                        <Queue size={8}>
                                            <Stack size={5}>
                                                <Text width={'auto'} textAlign={'center'} role={'black'} type={'callout'}>Шалгалтын дүн</Text>
                                                <Queue size={2}>
                                                    <DocumentIcon />
                                                    <Text bold role={'caution400'} type={'headline'}>{Number.parseInt(percent.toString())}%</Text>
                                                </Queue>
                                            </Stack>
                                            <Box height={53} width={2} role={'primary300'} alignSelf={'center'}></Box>
                                            <Stack size={5}>
                                                <Text width={'auto'} textAlign={'center'} role={'black'} type={'callout'}>Зөв хариулсан</Text>
                                                <Queue size={2}>
                                                    <DocumentIcon />
                                                    <Text bold role={'black'} type={'headline'}>{score} / {totalScore} </Text>
                                                </Queue>
                                            </Stack>
                                        </Queue>
                                    </Box>
                                </Border>
                            </AdmissionProcessCard>
                        </Stack>
                    </Box>
                )}
            </Box>
            <Box position={'relative'} flex={2} width={'90%'} alignItems={'center'} justifyContent={'center'}>
                <Stack size={5}>
                    {score / totalScore >= 0.6 ? (
                        <Box>
                            <Stack size={5}>
                                <Box width={'200%'} justifyContent={'center'} alignSelf={'center'} alignItems={'center'}>
                                    <Button category={'ghost'}
                                        onPress={() => navigation.navigate(NavigationRoutes.MainRoot)}
                                        width={'100%'}
                                    ><Text
                                        fontFamily={'Montserrat'}
                                        role={'primary500'}
                                        type={'callout'}
                                        textAlign={'center'}
                                        bold
                                    >HOME-руу буцах</Text></Button>
                                </Box>
                            </Stack>
                        </Box>
                    ) : (
                        <Box justifyContent={'center'} alignItems={'center'}>
                            <Button category={'fill'}
                                onPress={() => navigation.navigate(NavigationRoutes.MainRoot)}
                                width={344}
                            ><Text
                                fontFamily={'Montserrat'}
                                role={'white'}
                                type={'callout'}
                                textAlign={'center'}
                                bold
                            >HOME-руу буцах</Text>
                            </Button>
                        </Box>
                    )}
                </Stack>
            </Box>
        </Box>
    )
}
